import Foundation
import PDFKit
import Combine

protocol DocGenerator {
    func generatePDF() -> AnyPublisher<PDFDocument, Never>
    func getPDFdata() -> AnyPublisher<Data?, Never>
    var pages: [UIImage] { get set }
}

class PDFGenerator: NSObject, DocGenerator {
    var pages: [UIImage]
    private let pdfDocument = PDFDocument()
    private var pdfPage: PDFPage?
    
    var totalPages: Int { return pages.count }
    
    init(pages: [UIImage]) {
        self.pages = pages
    }
    
    func generatePDF() -> AnyPublisher<PDFDocument, Never> {
        return Future<PDFDocument, Never> { promise in
            DispatchQueue.global(qos: .background).async {
                for (index, image) in self.pages.enumerated() {
                    print("Scanned pages in generator: \(self.pages)")
                    self.pdfPage = PDFPage(image: image)
                    self.pdfDocument.insert(self.pdfPage!, at: index)
                }
                promise(.success(self.pdfDocument))
            }
        }.eraseToAnyPublisher()
    }
    
    func getPDFdata() -> AnyPublisher<Data?, Never> {
        return Future<Data?, Never> { promise in
            DispatchQueue.global(qos: .background).async { [weak self] in
                let data = self?.pdfDocument.dataRepresentation()
                promise(.success(data))
            }
        }.eraseToAnyPublisher()
    }
}
