import ProjectDescription

let infoPlist: [String: InfoPlist.Value] = [ // <1>
    "UILaunchScreen": [:]
]

let project = Project(
    name: "Scanverter",
    packages: [
        .package(url: "https://github.com/lucasbrown/swiftui-visual-effects.git", .upToNextMajor(from: "1.0.3")),
        .package(url: "https://github.com/exyte/Grid", .upToNextMajor(from: "1.1.0")),
        .package(url: "https://github.com/dartsyms/PopupView", .upToNextMajor(from: "0.0.12")),
        .package(url: "https://github.com/SwiftyTesseract/SwiftyTesseract.git", .upToNextMajor(from: "4.0.1"))
    ],
    targets: [
        Target(
            name: "Scanverter",
            platform: .iOS,
            product: .app,
            bundleId: "udev.dev.Scanverter",
            infoPlist: "Targets/Scanverter/Sources/Info.plist", //.extendingDefault(with: infoPlist),
            sources: [
                "Targets/Scanverter/Sources/**"
            ],
            resources: [
                "Targets/Scanverter/Resources/**"
            ],
            dependencies: [
                .package(product: "SwiftUIVisualEffects"),
                .package(product: "ExyteGrid"),
                .package(product: "PopupView"),
                .package(product: "SwiftyTesseract")
            ]
        )
    ]
)
