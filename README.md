## ScanverterApp

* captures photos (vision kit vs plain camera as service) 
* converts them to pdf (and also saves them as images in photo library) 
* text recognition with tesseract (vision kit's one disabled)
* SwiftUI with Combine
* SwiftyTesseract, ExyteGrid
* MVVM architecture for presentation layer
* SOA for business logic
* translation (google) service disabled
* not finished yet
* iOS 14

### Screenshots
<table>
  	<tr>
  		<td><img src="./screenshots/main_screen_with_folders.png" width=200 /></td>
		<td><img src="./screenshots/folder_details.png" width=200 /></td>
		<td><img src="./screenshots/settings_stubs.png" width=200 /></td>
		<td><img src="./screenshots/faceid_permission_request.png" width=200 /></td>
	</tr>
	<tr>
		<td><img src="./screenshots/biomethric_on_folder_locking.png" width=200 /></td>
		<td><img src="./screenshots/camera_screen.png" width=200 /></td>
		<td><img src="./screenshots/tesseract_recognition.png" width=200 /></td>
		<td><img src="./screenshots/recognition_result.png" width=200 /></td>
	</tr>
	<tr>
		<td><img src="./screenshots/visionkit_scan.png" width=200 /></td>
		<td><img src="./screenshots/visionkit_result.png" width=200 /></td>
		<td><img src="./screenshots/visionkit_pdf_view.png" width=200 /></td>
	</tr>
</table>

### Scanverter Project is managed by Tuist Tool

You can follow the steps below to get things done. Or just run the script from the root of the project.

Make sure you have *curl* and *bash* installed. 

```shell
./setup_project.sh
```

## What's Tuist 🕺

Tuist is a command line tool that helps you **generate**, **maintain** and **interact** with Xcode projects.

It's open source and written in Swift.

## Install ⬇️

### Running script (Recommended)

```shell
bash <(curl -Ls https://install.tuist.io)
```

## Bootstrap your first project 🌀

```bash
tuist init --platform ios
tuist generate # Generates Xcode project & workspace
tuist build # Builds your project
```
# or just generate and open it if you have one

```bash
tuist generate --open
```
[Check out](https://docs.tuist.io) the project "Get Started" guide to learn more about Tuist and all its features.

## 
